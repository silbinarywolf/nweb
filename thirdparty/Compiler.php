<?php

require_once(dirname(__FILE__) . "/Misc.php");
require_once(dirname(__FILE__) . "/Token.php");
require_once(dirname(__FILE__) . "/Lexer.php");
require_once(dirname(__FILE__) . "/AST.php");
require_once(dirname(__FILE__) . "/HTML.php");
require_once(dirname(__FILE__) . "/Parser.php");
require_once(dirname(__FILE__) . "/CodeGenerator.php");
require_once(dirname(__FILE__) . "/CompiledObjects.php");

class Compiler {
	// Config
	protected static $config_directories = array();

	// Reserved keywords
	protected static $keywords = array("nweb", "children", "parent", "siblings", "self", "global");

	protected static $current_filename = ""; // todo(Jake): remove if remains unused

	// AST Lookups
	protected static $components = array();

	protected static $enums = array();

	//
	protected static $styles_to_compile_for_components = array(); // array of Compiled_Component

	// Cached Compilations
	protected static $compiled_components = array();

	protected static $compiled_enums = array();

	// Misc
	public static $total_compile_time = 0;

	public static $dev_mode = true;

	public static function compile()
	{
		self::$compiled_components = array();

		// If debugging file has tokens/AST data, only use that.
		$ast = self::parseFile(dirname(__FILE__)."/../test/debug_feature.nweb.cpp");
		if ($ast === false)
		{
			foreach (self::$config_directories as $directory)
			{
	    		$files = scandir($directory);
	    		foreach ($files as $file)
	    		{
	    			if (substr($file, -4) == ".cpp")
	    			{
	    				$ast_file = self::parseFile($directory.$file);
	    				if (!empty($ast_file->layout))
	    				{
	    					// Todo(Jake): Split so it loads each layout sep.
	    					if (substr($file, 0, 5) == "home.")
	    					{
	    						$ast = $ast_file; 
	    					}
	    				}
	    			}
	    		}
			}
		}

		// Debug, note(Jake): remove when done
		if (isset($_GET["debug_ast"]))
		{
			var_dump($ast_nav);
			var_dump($ast); exit;
		}
		
		$keep_time = microtime(true);
		foreach ($ast->force_compile_components as $component_name)
		{
			self::compileComponent($component_name, null);
		}

		$html_root = self::compileLayout($ast->layout);

		// Compile the styles of each component
		foreach (self::$styles_to_compile_for_components as $compiled_component)
		{
			self::compileComponentStyles($compiled_component);
		}

		self::$total_compile_time += (microtime(true) - $keep_time);
?>
<head>
	<style>
	<?php echo CodeGenerator::generateCSS(self::$compiled_components); ?>
	</style>
</head>
<?php 
	$Games = array("The first nav", "The second nav");
	$html_code = CodeGenerator::generateHTML($html_root);
	eval("?>".$html_code);
?>
<?php

		echo "Lexing time: " . Lexer::$total_lexing_time . " seconds.<br/>";
		echo "Parsing time: " . Parser::$total_parse_time . " seconds.<br/>";
		//echo "Compile (Component only) time: " . self::$total_component_compile_time . " seconds.<br/>";
		echo "Compile time: " . self::$total_compile_time . " seconds.<br/>";
		echo "Total Compile time: " . (Lexer::$total_lexing_time+Parser::$total_parse_time+self::$total_compile_time) . " seconds.<br/>";
	}

	public static function clearDirectories()
	{
		self::$config_directories = array();
	}

	public static function addDirectory($directory)
	{
		self::$config_directories[] = $directory;
	}

	protected static function parseFile($filename)
	{
		// Lex into tokens
		$lexer = new Lexer();
		$tokens = $lexer->lexFile($filename);
		if ($tokens === false)
		{
			return false;
		}

		$ast_layout = null;
		$ast_components = null;

		$ast_file = Parser::parse($tokens, $filename);
		if ($ast_file == false)
		{
			return false;
		}

		self::$current_filename = $filename;

		// Put components into global components list
		foreach ($ast_file->components as $component)
		{
			if (isset(self::$components[$component->name]))
			{
				// todo(Jake): make error message more useful (ie. show the two locations)
				$component_first = self::$components[$component->name];
				self::compileError("Component cannot be declared twice in the same codebase. " . $component->name . " declared twice.");
				return false;
			}
			else
			{
				self::$components[$component->name] = $component;
			}	
		}

		// Put enums into global enums list
		foreach ($ast_file->enums as $enum)
		{
			if (isset(self::$components[$enum->name]))
			{
				self::compileError("Enum cannot have same name as component in the same codebase. " . $enum->name . " declared twice.");
			}
			else if (isset(self::$enums[$enum->name]))
			{
				self::compileError("Enum cannot be declared twice in the same codebase. " . $enum->name . " declared twice.");
			}
			else
			{
				self::$enums[$enum->name] = $enum;
			}
		}
		return $ast_file;
	}

	public static function compute(Token $lvar, Token $rvar, $operator_token_id)
	{
		$lvalue = $lvar->string;
		$rvalue = $rvar->string;
		$result = null;

		if (is_string($lvalue))
		{
			// Remember current suffix
			$suffix_1 = substr($lvalue, -1);
			$suffix_2 = substr($lvalue, -2);
			$suffix_3 = substr($lvalue, -3);
		}

		switch ($lvar->token_id)
		{

			// todo(Jake): convert hex to real and vice versa properly
			case Token::TOK_EXPR_REAL:
			case Token::TOK_EXPR_REAL_HEX:
				switch ($operator_token_id)
				{
					case Token::TOK_EXPR_ADD:
						$result = $lvalue + $rvalue;
					break;

					case Token::TOK_EXPR_SUB:
						$result = $lvalue - $rvalue;
					break;

					case Token::TOK_EXPR_MULT:
						$result = $lvalue * $rvalue;
					break;

					case Token::TOK_EXPR_DIV:
						$result = $lvalue / $rvalue;
					break;

					case Token::TOK_EXPR_MOD:
						$result = $lvalue % $rvalue;
					break;

					case Token::TOK_EXPR_COND_EQUAL:
						$result = ($lvalue == $rvalue);
					break;

					case Token::TOK_EXPR_COND_NOT_EQUAL:
						$result = ($lvalue != $rvalue);
					break;

					case Token::TOK_EXPR_COND_ABOVE_EQUAL:
						$result = ($lvalue >= $rvalue);
					break;

					case Token::TOK_EXPR_COND_BELOW_EQUAL:
						$result = ($lvalue <= $rvalue);
					break;

					case Token::TOK_EXPR_COND_ABOVE:
						$result = ($lvalue > $rvalue);
					break;

					case Token::TOK_EXPR_COND_BELOW:
						$result = ($lvalue < $rvalue);
					break;

					case Token::TOK_EXPR_COND_AND:
						$result = ($lvalue && $rvalue);
					break;

					case Token::TOK_EXPR_COND_OR:
						$result = ($lvalue || $rvalue);
					break;

					default:
						self::assert("Compiler::compute:Unknown expression token. token id = " . $operator_token_id);
						return null;
					break;
				}
			break;

			case Token::TOK_EXPR_STR:
				switch ($operator_token_id)
				{
					case Token::TOK_EXPR_ADD:
						$result = $lvalue . $rvalue;
					break;

					case Token::TOK_EXPR_COND_EQUAL:
						$result = ($lvalue == $rvalue);
					break;

					case Token::TOK_EXPR_COND_NOT_EQUAL:
						$result = ($lvalue != $rvalue);
					break;

					case Token::TOK_EXPR_COND_ABOVE_EQUAL:
					case Token::TOK_EXPR_COND_BELOW_EQUAL:
					case Token::TOK_EXPR_COND_ABOVE:
					case Token::TOK_EXPR_COND_BELOW:
						self::compileError("Invalid use of '".Token::operatorToString($operator_token_id)."', cannot be used with strings.", $lvalue);
					break;

					default:
						self::assert("Compiler::compute:Invalid operator on string '".Token::operatorToString($operator_token_id)."' . token id = " . $operator_token_id);
						return null;
					break;
				}
			break;

			case Token::TOK_EXPR_BACKEND_VAR:
				$asf = $sf; // force error for stack trace, todo(Jake): remove
				self::assert("Compiler::compute:Invalid variable type, backend variable cannot be computed, value = " . $lvar->string);
				return false;
			break;

			default:
				self::assert("Compiler::compute:Invalid variable type, token = ".$lvar->token_id.", value = " . $lvar->string);
				return null;
			break;
		}
		// Re-apply suffix if not compared with logical operator
		if ($operator_token_id != Token::TOK_EXPR_COND_EQUAL 
			&& $operator_token_id != Token::TOK_EXPR_COND_AND
			&& $operator_token_id != Token::TOK_EXPR_COND_OR)
		{
			if (isset($suffix_3) && $suffix_3 === "rem") {
				$result .= $suffix_3;
			} else if (isset($suffix_2) && ($suffix_2 === "px" || $suffix_2 === "em")) {
				$result .= $suffix_2;
			} else if (isset($suffix_1) && ($suffix_1 === "%")) {
				$result .= $suffix_1;
			}
		}
		$lvar = clone $lvar;
		$lvar->string = $result;
		return $lvar;
	}

	public static function evaluateVariable(Token $token, array $variable_set)
	{
		if ($token->token_id != Token::TOK_EXPR_VAR && $token->token_id != Token::TOK_EXPR_CSS_VAR)
		{
			self::assert("Using 'evaluateVariable' on non-variable token");
			return null;
		}
		$string = $token->string;
		if (!isset($variable_set[$string]))
		{
			// If variable not found, split up string so that
			// 'children.count' becomes 'children' and 'count'
			// and check for object form.
			$variable_scope = explode(".", $string);
			if (isset($variable_set[$variable_scope[0]]))
			{
				// local scope
				$object_var = $variable_set[$variable_scope[0]];
			}
			else if (isset(self::$enums[$variable_scope[0]]))
			{
				// global scope
				$ast_enum = self::$enums[$variable_scope[0]];
				if (!isset($ast_enum->variables[$variable_scope[1]]))
				{
					self::compileError("Variable '".$variable_scope[1]."' does not exist on '".$variable_scope[0]."' enum.", $token);
					return null;
				}
				$compiled_enum = self::compileEnum($ast_enum);
				return $compiled_enum[$variable_scope[1]];
			}
			else
			{
				self::compileError("Variable has not been declared '".$variable_scope[0]."'.", $token);
				return null;
			}

			// If not an object, throw compile error
			if ($object_var->token_id != Token::TOK_EXPR_OBJECT)
			{
				self::compileError("Cannot use '.' on non-object variable '".$object_name."'.", $token);
				return null;
			}

			if (!isset($object_var->string[$variable_scope[1]]))
			{
				self::compileError("Variable '".$variable_scope[1]."' does not exist on '".$variable_scope[0]."'.", $token);
				return null;
			}
			return $object_var->string[$variable_scope[1]];
		}
		return $variable_set[$string];
	}

	public static function isBackendExpression(array $expr_tokens, array $vars = array())
	{
		foreach ($expr_tokens as $token)
		{
			if ($token->token_id == Token::TOK_EXPR_VAR)
			{
				// Check if variable is pointing to a backend variable
				$token_ret = self::evaluateVariable($token, $vars);
				if ($token_ret->token_id == Token::TOK_EXPR_BACKEND_VAR || $token_ret->token_id == Token::TOK_EXPR_BACKEND_EXPR)
				{
					return true;
				}
			}
			else if ($token->token_id == Token::TOK_EXPR_BACKEND_VAR || $token->token_id == Token::TOK_EXPR_BACKEND_EXPR)	
			{
				return true;
			}
		}
		return false;
	}

	public static function evaluateExpression(array $expr_tokens, array $vars = array(), array &$variables_touched = null)
	{
		if (self::isBackendExpression($expr_tokens, $vars))
		{
			$expr_arr = array();
			$first_token = reset($expr_tokens);
			foreach ($expr_tokens as $token)
			{
				if ($token->token_id >= Token::TOK_EXPR_SET && $token->token_id <= Token::TOK_EXPR_END)
				{
					$rval = array_pop($stack);
					$expr_arr[] = array_pop($stack); // $lval
					$expr_arr[] = $token;
					$expr_arr[] = $rval;
				}
				else
				{
					// Retrieve variable
					if ($token->token_id == Token::TOK_EXPR_VAR)
					{
						$token_ret = self::evaluateVariable($token, $vars);
						if ($token_ret === null)
						{
							// if compile error
							return null;
						}
						if ($variables_touched !== null)
						{
							$variables_touched[$token->string] = $token_ret;
						}
						$stack[] = $token_ret;
					}
					else if ($token->token_id == Token::TOK_EXPR_BACKEND_VAR)
					{
						$stack[] = $token;
					}
					else
					{
						$stack[] = $token;
					}
				}
			}
			if (count($stack) == 1)
			{
				return $first_token;
			}
			if (count($stack) > 1)
			{
				return self::assert("Unable to evaluate backend expression. (" . $expr_string . ")");
			}
			$ret_token = clone $first_token;
			$ret_token->token_id = Token::TOK_EXPR_BACKEND_EXPR;
			$ret_token->string = $expr_arr;

			return $ret_token;
		}
		else
		{
			foreach ($expr_tokens as $token)
			{
				if ($token->token_id >= Token::TOK_EXPR_SET && $token->token_id <= Token::TOK_EXPR_END)
				{
					$rval = array_pop($stack);
					$lval = array_pop($stack);
					// Retrieve variables
					if ($lval->token_id == Token::TOK_EXPR_VAR)
					{
						if ($variables_touched !== null)
						{
							$variables_touched[$lval->string] = true;
						}
						$token_ret = self::evaluateVariable($lval, $vars);
						if ($token_ret === null)
						{
							// if compile error
							return null;
						}
						if ($variables_touched !== null)
						{
							$variables_touched[$lval->string] = $token_ret;
						}
					}
					if ($rval->token_id == Token::TOK_EXPR_VAR)
					{
						$token_ret = self::evaluateVariable($rval, $vars);
						if ($token_ret === null)
						{
							// if compile error
							return null;
						}
						if ($variables_touched !== null)
						{
							$variables_touched[$rval->string] = $token_ret;
						}
					}
					$stack[] = self::compute($lval, $rval, $token->token_id);
				}
				else
				{
					// Retrieve variable
					if ($token->token_id == Token::TOK_EXPR_VAR)
					{
						$token_ret = self::evaluateVariable($token, $vars);
						if ($token_ret === null)
						{
							// if compile error
							return null;
						}
						if ($variables_touched !== null)
						{
							$variables_touched[$token->string] = $token_ret;
						}
						$stack[] = $token_ret;
					}
					else
					{
						$stack[] = $token;
					}
				}
			}
			if (count($stack) > 1 || count($stack) == 0)
			{
				return self::assert("Unable to evaluate expression.");
			}
			return $stack[0];
		}
	}

	public static function evaluateCSSExpression(array $tokens, array $vars = array(), array &$variables_touched = null)
	{
		$value = "";
		$in_function_call = false;
		foreach ($tokens as $token)
		{
			switch ($token->token_id)
			{
				case Token::TOK_EXPR_CSS_BACKEND_VAR:
					return self::compileError("Cannot use backend variables in 'styles' block. name = $" . $token->string, $token);
				break;

				case Token::TOK_EXPR_CSS_VAL:
					$value .= " " . $token->string;
					if ($in_function_call) {
						$value .= ",";
					}
				break;

				case Token::TOK_EXPR_CSS_VAR:
					// Retrieve variable value
					$token_ret = self::evaluateVariable($token, $vars);
					if ($token_ret === null)
					{
						// if compile error
						self::assert("Undeclared variable '".$token->string."' in CSS block at Line " . $token->line_number);
						return null;
					}
					if ($variables_touched !== null)
					{
						$variables_touched[$token->string] = $token_ret;
					}
					$value .= " " . $token_ret->string;
					if ($in_function_call) {
						$value .= ",";
					}
				break;

				case Token::TOK_EXPR_CSS_FUNC_BEGIN:
					$value .= $token->string."(";
					if ($in_function_call)
					{
						self::assert("CSS doesn't have functions within functions.");
					}
					$in_function_call = true;
				break;

				case Token::TOK_EXPR_CSS_FUNC_END:
					$value .= ")";
					$in_function_call = false;
				break;

				default:
					return self::compileError("Invalid token id in expression. token id = " . $token->token_id . ", token string = " . $token->string);
				break;
			}
		}
		return ltrim($value);
	}

	protected static function compileEnum(AST_Enum $ast_enum)
	{
		$name = $ast_enum->name;
		if (!isset(self::$compiled_enums[$name]))
		{
			$compiled_enum = array();
			foreach ($ast_enum->variables as $name => $ast_code_var)
			{
				$compiled_enum[$name] = self::evaluateExpression($ast_code_var->expr_tokens);
			}
			self::$compiled_enums[$name] = $compiled_enum;
			return $compiled_enum;
		}
		return self::$compiled_enums[$name];
	}

	// Note: parent_component is passed into components used in a components 'layout' block
	protected static function compileComponent($name, AST_Block $parameters = null, AST_Layout $ast_layout = null, Compiled_Component $parent_component = null)
	{
		if (isset(self::$components[$name]))
		{
			//var_dump(array("param" => $parameters));
			//var_dump(array("#".$name => $ast_layout));
			$keep_time = microtime(true);
			$component = self::$components[$name];

			// todo(Jake): make use of parent/sibling variables change UID key
			$uid = '';

			// Compile variables (without parameters passed)
			$variables_codeblock = null;
			if ($component->variables !== null) 
			{
				// todo(Jake): add constraint so only variables can be in 'variables', 'if/while' blocks not allowed
				$variables_codeblock = self::compileCodeBlock($component->variables);
				if ($variables_codeblock === null)
				{
					// Return if hit error
					return false;
				}
			}



			// Compile parameters and override variables
			$parameters_codeblock = null;
			if ($parameters !== null)
			{
				// Compile the base component (parameterless), this allows the compiler to get information
				// about the default variables during 'compileComponentStyles' and
				// determine the selector for components during 'compileStyles'
				if (!isset(self::$compiled_components[$name][""]))
				{
					self::compileComponent($name, null, null);
				}

				if ($variables_codeblock == null)
				{
					self::compileError("Cannot use parameters if no variables declared in 'variables' block.", $ast_layout);
					return false;
				}
				if ($parent_component !== null && $parent_component->variables_codeblock !== null)
				{
					// Allow parent component variables to be passed in with parameters
					// NOTE: Variables passed in take precedence over local scope
					$variables = array_merge($variables_codeblock->variables, $parent_component->variables_codeblock->variables);
				}
				else
				{
					$variables = $variables_codeblock->variables;
				}

				$parameters_codeblock = self::compileCodeBlock($parameters, $variables);
				if ($parameters_codeblock === null)
				{
					// Return if hit error
					return false;
				}
				foreach ($parameters_codeblock->variables as $varname => $token)
				{
					if (isset($variables_codeblock->variables[$varname]))
					{
						$variables_codeblock->variables[$varname] = $token;
					}
					else
					{
						self::compileError("Cannot override variable '".$varname."' for '".$name."' unless it is declared in the 'variables' block.", $ast_layout);
						return false;
					}
				}

				

				// Add parameters to UID, this is so already compiled components
				// can be cached during compilation.
				ksort($parameters_codeblock->variables, SORT_STRING);
				$uid .= "(";
				foreach ($parameters_codeblock->variables as $varname => $eval_token)
				{
					$uid .= $varname."=".$eval_token->token_id."|".$eval_token->string . ",";
				}
				$uid = substr($uid, 0, -1);
				$uid .= ")";
			}


			// If not already compiled, complete compilation process
			if (!isset(self::$compiled_components[$name][$uid]))
			{
				// Get children
				$children = array();
				if ($ast_layout !== null)
				{
					$children = array();
					foreach ($ast_layout->nodes as $node)
					{
						if ($node->type != AST_Node::IFBLOCK || 
							$node->type != AST_Node::ELSEBLOCK ||
							$node->type != AST_Node::ELSEIFBLOCK)
						{
							$children[] = $node;
						}
					}
				}

				// Run code on variables, default or passed-in.
				if ($component->calculate !== null)
				{
					$overriden_variables_codeblock = self::compileCodeBlock($component->calculate, $variables_codeblock->variables);
					foreach ($overriden_variables_codeblock->variables as $varname => $token)
					{
						if (isset($variables_codeblock->variables[$varname]))
						{
							$variables_codeblock->variables[$varname] = $token;
						}
						else
						{
							self::compileError("Undeclared variable '".$varname."' in '".$name."' components 'calculate' block.", $ast_layout);
							return false;
						}
					}
				}

				// todo(Jake): just make everything all the variables process on the component
				$compiled_component = new Compiled_Component();

				// Evaluate attributes
				$attributes = array();
				if ($component->attributes !== null) 
				{
					if ($variables_codeblock === null)
					{
						$attributes_codeblock = self::compileCodeBlock($component->attributes);
					}
					else
					{
						$attributes_codeblock = self::compileCodeBlock($component->attributes, $variables_codeblock->variables);
					}
					if ($attributes_codeblock == null) {
						// Stop due to error
						return null;
					}
					// Remove blank attributes
					foreach ($attributes_codeblock->variables as $attribute => $token)
					{
						if ($token->string != "")
						{
							$attributes[$attribute] = $token->string;
						}
					}
					if (isset($attributes["element"]))
					{
						$compiled_component->element = $attributes["element"];
						unset($attributes["element"]);
					}
				}

				$compiled_component->name = $name;
				$compiled_component->uid = $uid;
				$compiled_component->variables_codeblock = $variables_codeblock;
				$compiled_component->parameters_codeblock = $parameters_codeblock;
				$compiled_component->attributes = $attributes;

				// Add 'children' object
				$children_tok = new Token();
				$children_tok->token_id = Token::TOK_EXPR_OBJECT;
				$children_tok->string = array();
				$children_tok->line_number = $component->line_number;
				// Add 'count' to 'children' object
				$children_count_tok = new Token();
				$children_count_tok->token_id = Token::TOK_EXPR_REAL;
				$children_count_tok->string = count($children);
				$children_tok->string["count"] = $children_count_tok;
				$compiled_component->builtin_variables["children"] = $children_tok;

				self::$compiled_components[$name][$uid] = $compiled_component;
				self::$styles_to_compile_for_components[] = $compiled_component;

				// Compile layout once the current component has been compiled
				if ($component->layout !== null) 
				{
					//var_dump(array($name => $component->layout));
					$compiled_component->layout = self::compileLayout($component->layout, $compiled_component);
					// Detect compilation error
					if ($compiled_component->layout == false)
					{
						return false;
					}
				}
				//self::$total_component_compile_time += (microtime(true) - $keep_time);
				return $compiled_component;
			}
			else
			{
				//self::$total_component_compile_time += (microtime(true) - $keep_time);
				return self::$compiled_components[$name][$uid];
			}
		}
		else
		{
			self::compileError("Undeclared component '".$name."' used in layout.", $ast_layout);
			return false;
		}
		return false;
	}

	protected static function compileComponentStyles(Compiled_Component $compiled_component) {
		// Evaluate styles
		// todo(Jake): Defer compiling styles till the end to determine
		//			   what styles to throwout or not, etc. (CSS optimization)
		if (!empty($compiled_component->styles))
		{
			self::assert("Cannot compile component styles twice.");
		}

		$component = self::$components[$compiled_component->name];

		// Evaluate styles
		$compiled_component->styles = null;
		$compiled_component->styles_variables_touched = array();
		if ($component->styles !== null) 
		{
			if ($compiled_component->variables_codeblock === null)
			{
				$compiled_component->styles = self::compileStyles($component->styles);
			}
			else
			{
				$compiled_component->styles = self::compileStyles($component->styles, $compiled_component->variables_codeblock->variables);
			}

			// Get variables accessed from in CSS styling
			foreach ($compiled_component->styles as $conditional_selector => $selectors)
			{
				foreach ($selectors as $selector => $compiled_codeblock)
				{
					$compiled_component->styles_variables_touched = array_merge($compiled_component->styles_variables_touched, $compiled_codeblock->variables_touched);
				}
			}

			// Compile the base component, this allows the compiler to get information
			// about the default variables
			// ie. No parameters
			$base_compiled_component = null;
			if ($compiled_component->parameters_codeblock !== null)
			{
				if (isset(self::$compiled_components[$compiled_component->name][""]))
				{
					$base_compiled_component = self::$compiled_components[$compiled_component->name][""];
				}
				else
				{
					self::assert("The base compiled component should be compiled earlier.");
				}
			}

			// Diff' styles with base component
			if ($base_compiled_component !== null) 
			{
				$base_styles = $base_compiled_component->styles;
				foreach ($compiled_component->styles as $conditional_selector => $selectors)
				{
					foreach ($selectors as $selector => $compiled_codeblock)
					{
						if (isset($base_styles[$conditional_selector][$selector]))
						{
							$base_styles_variables = $base_styles[$conditional_selector][$selector]->variables;
						}
						else
						{
							$base_styles_variables = null;
						}
						foreach ($compiled_codeblock->variables as $varname => $value)
						{
							// If the property is set on the base component and is the same
							// value, then don't use it on the parameterized component
							if (isset($base_styles_variables[$varname]) &&
								$base_styles_variables[$varname] == $value)
							{
								unset($compiled_codeblock->variables[$varname]);
							}
						}
					}
				}
			}
		}

		// Determine class attribute from UID information
		// ie. turn 'self' into '.col' or '.col--width-10px--height-30px'
		if (!empty($compiled_component->attributes["class"]))
		{
			$base_classname = strtolower($compiled_component->attributes["class"]);
		}
		else
		{
			$base_classname = "";
		}
		$param_classname = "";
		if ($compiled_component->parameters_codeblock !== null && $component->styles !== null)
		{
			//var_dump($compiled_component->styles_variables_touched);

			// If current component is given parameters, get component styles touched
			// when no parameters are passed. ie, the base component.
			if ($base_compiled_component != null)
			{
				$base_styles_vars_touched = $base_compiled_component->styles_variables_touched;
			}
			foreach ($compiled_component->styles_variables_touched as $varname => $token)
			{
				// Only add property to classname if base component doesn't have it set
				// or it hasn't changed from the default value.
				if (!isset($base_styles_vars_touched[$varname]) || 
					$base_styles_vars_touched[$varname]->string != $token->string)
				{
					static $sanitizer_items = array(
						".", " ", "#"
					);
					$value_sanitized = str_replace($sanitizer_items, "_", $token->string);
					$param_classname .= "--".$varname."-".$value_sanitized;
				}
			}
		}
		$compiled_component->attributes["class"] = $base_classname;
		if (!empty($param_classname))
		{
			// eg. --content_width-980px--color--_1A1B1F
			$compiled_component->attributes["class"] .= " " . $base_classname . $param_classname;
		}
	}

	protected static function compileLayout(AST_Layout $ast_layout, Compiled_Component $component = null)
	{
		if ($component === null)
		{
			$root_node = new HTML_Element();
			$root_node->element = "root";
		}
		else
		{
			$root_node = new HTML_Element_Component();
			$root_node->element = "root:component";
			$root_node->layout_nodes = null;
			$root_node->component = $component;
		}

		// Helper stack, when the $layout_stack has null values inside, you pop
		// from this stack to run 'closing tag' logic.
		$parent_stack = array();
		$parent_stack[] = $root_node;

		// If above 0, force that if/else/elseif to use backend variables
		// this is incremented at the AST_Node::IFBLOCK case and decremented
		// per AST_Node::ELSE/ELSEIF case.
		$force_backend_ifelse = 0;

		// Append children in reverse
		$layout_stack = array();
		$index = count($ast_layout->nodes);
		while($index) {
			$layout_stack[] = $ast_layout->nodes[--$index];
		}

		while (count($layout_stack) > 0)
		{
			$ast_node = array_pop($layout_stack);
			$html_node = end($parent_stack); 

			// If closing tag
			if ($ast_node == null)
			{
				if (count($parent_stack) == 1)
				{
					self::assert("root element cannot be popped from layout stack.", $ast_node);
					return false;
				}
				$html = array_pop($parent_stack);
				// Inserts child nodes into element groups
				if ($html->type == HTML_Object::ELEMENT_COMPONENT)
				{
					if (empty($html->children_refs))
					{
						$html->nodes = $html->layout_nodes;
						$html->layout_nodes = null;

						// If using 'children' and there is no 'children' keyword found, throw error
						$children_count = (int)$html->component->builtin_variables["children"]->string["count"]->string;
						if ($children_count > 0)
						{
							self::compileError("Unused keyword 'children', 'children' must be present in a component 'layout' block.");
							return false;
						}
					}
					else
					{
						if (!empty($html->nodes))
						{
							foreach ($html->children_refs as $nref)
							{
								// Remove 'children' keyword placeholder
								unset($nref->ref[$nref->ref_ind]);
								// Insert $html->nodes into the layout_nodes
								array_splice($nref->ref, $nref->ref_ind, 0, $html->nodes);
							}
						}
						else
						{
							// Don't attempt to splice in content if there is none as
							// that can mess with the array keys, therefore nullifying
							// the $nref->ref_ind
							foreach ($html->children_refs as $nref)
							{
								// Remove 'children' keyword placeholder
								unset($nref->ref[$nref->ref_ind]);
							}
						}
						// After insertion into the layout is complete (via $nref->ref), move
						// to $html->nodes
						$html->nodes = $html->layout_nodes;

						// todo(Jake): remove these if they're unnecessary
						unset($html->layout_nodes);
						unset($html->children_refs);
					}
				}
				// don't bother processing layout_node as it's 'null'
				// in this case
				continue;
			}

			switch ($ast_node->type)
			{
				case AST_Node::ELEMENT:
					if (isset($ast_node->name[0]) && ctype_upper($ast_node->name[0])) 
					{
						$compiled_component = self::compileComponent($ast_node->name, $ast_node->parameters, $ast_node, $component);
						if ($compiled_component === false || $compiled_component === null)
						{
							self::compileError("Unable to compile component '".$ast_node->name."'.", $ast_node);
							return false;
						}


						if ($compiled_component->layout === null)
						{
							// If not using 'layout' block
							// NOTE: Attributes are needed to be given by reference so that
							//	     when CSS styles are compiled, the class can be modified.
							$html = new HTML_Element();
							$html->element = &$compiled_component->element;
							$html->attributes = &$compiled_component->attributes;
							$html_node->nodes[] = $html;
						}
						else
						{
							// NOTE: Only root node will be copied, which is all that is needed for 'children' keyword
							$html = clone $compiled_component->layout;
							$html->layout_nodes = $html->nodes;
							$html->nodes = null; // NOTE: Required to be null for below forloop
							foreach ($html->children_refs as $nref)
							{
								if ($nref->ref == null)
								{
									// NOTE: If ref is pointing to the root node, when $this->nodes is
									//		 reset, the children_ref is also lost, so if the children_ref is null
									//		 after resetting the nodes, point children_ref to the layout_nodes
									unset($nref->ref);
									$nref->ref = &$html->layout_nodes;
								}
							}
							$html->nodes = array();
							$html_node->nodes[] = $html;
						}

						$layout_stack[] = null;
						$parent_stack[] = $html;
					}
					else if ($ast_node->name == "self")
					{
						if ($component == null)
						{
							self::compileError("Cannot reference 'self' in global layout. Can only be referenced inside a component 'layout' block.", $ast_node);
							return false;
						}

						// NOTE: Attributes are needed to be given by reference so that
						//	     when CSS styles are compiled, the class can be modified.
						$html = new HTML_Element();
						$html->element = &$component->element;
						$html->attributes = &$component->attributes;
						$html_node->nodes[] = $html;

						$layout_stack[] = null;
						$parent_stack[] = $html;
					}
					else
					{
						self::compileError("Invalid keyword '".$ast_node->name."' in layout .", $ast_node);
						return false;
					}

					// Append children in reverse
					$index = count($ast_node->nodes);
					while($index) {
						$layout_stack[] = $ast_node->nodes[--$index];
					}
				break;

				case AST_Node::LOOPREPEAT:
					$loop_count = (int)$ast_node->amount;
					for ($i = 0; $i < $loop_count; ++$i)
					{
						// Append children in reverse
						$index = count($ast_node->nodes);
						while($index) {
							$layout_stack[] = $ast_node->nodes[--$index];
						}
					}
				break;

				case AST_Node::LOOPFOREACH:
					$html = new HTML_Backend_Foreach();
					$html->array_name = $ast_node->array_name;
					$html->value_name = $ast_node->value_name;
					$html_node->nodes[] = $html;

					$layout_stack[] = null;
					$parent_stack[] = $html;

					// Append children in reverse
					$index = count($ast_node->nodes);
					while($index) {
						$layout_stack[] = $ast_node->nodes[--$index];
					}
				break;

				// todo(Jake): Force `if` and subsequent `else if` to use backend
				//             if at least one `else if` uses a backend variable
				case AST_Node::IFBLOCK:
				case AST_Node::ELSEIFBLOCK:
					if ($component == null)
					{
						self::compileError("'if' keyword not implemented for global layout yet.", $ast_node);
						return false;
					}
					$existing_variables = array_merge($component->variables_codeblock->variables, $component->builtin_variables);
					$value = self::evaluateExpression($ast_node->expr_tokens, $existing_variables);
					if (!is_object($value))
					{
						self::compileError("null variable returned from expression in if-block.", $ast_node);
						return null;
					}
					if ($force_backend_ifelse > 0 || ($value->token_id == Token::TOK_EXPR_BACKEND_VAR || $value->token_id == Token::TOK_EXPR_BACKEND_EXPR))
					{
						// If using backend information, use backend if statement
						if ($layout_node->type == AST_Node::IFBLOCK) {
							$html = new HTML_Backend_If();
						} else if ($layout_node->type == AST_Node::ELSEIFBLOCK) {
							$html = new HTML_Backend_ElseIf(); 
						} else {
							self::assert("Invalid backend variable cannot if-block case.", $ast_node);
							return null;
						}
						$html->expr_tokens = $value->string;
						$html_node->nodes[] = $html;

						if ($ast_node->type == AST_Node::ELSEIFBLOCK)
						{
							--$force_backend_ifelse;
							if ($force_backend_ifelse < 0)
							{
								self::assert("Force `if` and subsequent `else if` to use backend if at least one `else if` uses a backend variable.", $ast_node);
								return null;
							}
						}
						else
						{
							// Detect future else/elseif cases from stack
							// and increment the `force_backend_ifelse
							$next_layout_node = end($layout_stack);
							while (is_object($next_layout_node) && 
								($next_layout_node->type == AST_Node::ELSEBLOCK || $next_layout_node->type == AST_Node::ELSEIFBLOCK))
							{
								$next_layout_node = prev($layout_stack);
								++$force_backend_ifelse; // See above decl. for its use.
							}
						}

						$layout_stack[] = null;
						$parent_stack[] = $html;

						// Append children in reverse
						$index = count($ast_node->nodes);
						while($index) {
							$layout_stack[] = $ast_node->nodes[--$index];
						}
					}
					else
					{
						// Put the if-statement nodes in front of the queue if the statement
						// is not equal to false.
						if ($value->string != false)
						{
							// Remove else/elseif cases from stack
							// todo(Jake): ensure future else/elseifs aren't backend-focused
							$next_layout_node = end($layout_stack);
							while ($next_layout_node != false && 
								($next_layout_node->type == AST_Node::ELSEBLOCK || $next_layout_node->type == AST_Node::ELSEIFBLOCK))
							{
								$next_layout_node = array_pop($layout_stack);
							}

							// Append children in reverse
							$index = count($ast_node->nodes);
							while($index) {
								$layout_stack[] = $ast_node->nodes[--$index];
							}
						}
					}
				break;

				case AST_Node::ELSEBLOCK:
					if ($force_backend_ifelse > 0)
					{
						$html = new HTML_Backend_Else();
						$html_node->nodes[] = $html;

						$layout_stack[] = null;
						$parent_stack[] = $html;

						--$force_backend_ifelse;
					}
					else if ($component == null)
					{
						self::compileError("'else' keyword not implemented for global layout yet.", $ast_node);
						return false;
					}

					// Else block only stays on the stack if the previous 'if/else-if' 
					// evaluated to false or if previous 'if/else-if' is backend.

					// Append children in reverse
					$index = count($ast_node->nodes);
					while($index) {
						$layout_stack[] = $ast_node->nodes[--$index];
					}
				break;

				case AST_Node::CONTENT:
					$html = new HTML_Content();
					$html->content = $ast_node->name; 
					$html_node->nodes[] = $html;
				break;

				case AST_Node::VARIABLE:
					if ($component === null)
					{
						self::compileError("Variables ({$layout_node->name}) cannot be in the global layout. Can only be referenced inside a component layout block.", $ast_node);
						return false;
					}
					$variable_name = $ast_node->name;
					if (isset($component->builtin_variables[$variable_name]))
					{
						switch ($variable_name)
						{
							case "children":
								$html = new HTML_Keyword();
								$html->keyword = $variable_name; 
								$html_node->nodes[] = $html;

								/*if ($root_node->children_ref !== null)
								{
									self::compileError("Cannot declare 'children' twice in components 'layout' block.", $ast_node);
									return false;
								}*/

								$nref = new NodeRef();
								$nref->ref = &$html_node->nodes;
								$nref->ref_ind = count($html_node->nodes)-1;;

								$root_node->children_refs[] = $nref;
							break;

							default:
								$variable = $component->builtin_variables[$ast_node->name];
								$html = new HTML_Content();
								$html->content = $variable->string; 
								$html_node->nodes[] = $html;
							break;
						}
					}
					else
					{
						if (!isset($component->variables_codeblock->variables[$variable_name]))
						{
							self::compileError("Undeclared variable '".$ast_node->name."' in '".$component->name."' components 'layout' block.", $ast_layout);
							return false;
						}
						$variable = $component->variables_codeblock->variables[$ast_node->name];
						switch ($variable->token_id)
						{
							case Token::TOK_EXPR_STR:
							case Token::TOK_EXPR_REAL:
							case Token::TOK_EXPR_REAL_HEX:
								$html = new HTML_Content();
								$html->content = $variable->string;
							break;

							case Token::TOK_EXPR_BACKEND_VAR:
								$html = new HTML_Backend_Variable();
								$html->name = $variable->string;
							break;

							default:
								self::assert("Invalid token found in layout, expected string/real/realhex.", $ast_layout);
								return false;
							break;
						}
						$html_node->nodes[] = $html;
					}
				break;

				case AST_Node::BACKEND_VARIABLE:
					$html = new HTML_Backend_Variable();
					$html->name = $ast_node->name;
					$html_node->nodes[] = $html;
				break;

				default:
					self::assert("Unsupported node '".$ast_node->name."' found in layout.", $ast_node);
					return false;
				break;
			}
		}
		return $root_node;
	}

	protected static function compileCodeBlock(AST_Block $ast_codeblock, array $existing_variables = array())
	{
		$compiled_codeblock = new Compiled_CodeBlock();
		//$compiled_codeblock->variables;
		//$compiled_codeblock->variables_touched;

		$codeblock = $ast_codeblock->nodes;

		$code_queue = array();
		$code_queue = $ast_codeblock->nodes;

		while (count($code_queue) > 0)
		{
			$code_node = array_shift($code_queue);

			switch ($code_node->type)
			{
				case AST_Node::BLOCK:
					self::assert("AST_Code::BLOCK is unsupported. type = '".$code_node->type."'", $ast_codeblock);
					return false;
				break;

				case AST_Node::VARIABLE:
					if (in_array($code_node->name, self::$keywords))
					{
						self::compileError("Cannot assign '".$code_node->name."' as it is a reserved keyword.", $code_node);
						return null;
					}
					$value = self::evaluateExpression($code_node->expr_tokens, $existing_variables, $compiled_codeblock->variables_touched);
					if (!is_object($value))
					{
						self::compileError("null variable returned from variable assign expression.", $code_node);
						return null;
					}
					/*if ($value->token_id == Token::TOK_EXPR_BACKEND_VAR || $value->token_id == Token::TOK_EXPR_BACKEND_EXPR)
					{
						self::assert("Backend variable cannot be used in variable setting.", $code_node);
						return null;
					}*/
					switch ($code_node->assign)
					{
						case AST_Code_Variable::SET:
							$compiled_codeblock->variables[$code_node->name] = $existing_variables[$code_node->name] = $value;
						break;

						case AST_Code_Variable::ADD:
							if (isset($existing_variables[$code_node->name]))
							{
								$variable = self::compute($existing_variables[$code_node->name], $value, Token::TOK_EXPR_ADD);
								$compiled_codeblock->variables[$code_node->name] = $existing_variables[$code_node->name] = $variable;
							}
							else
							{
								self::compileError("Cannot += on undeclared variable '".$code_node->name."'.", $code_node);
								return null;
							}
						break;

						default:
							self::assert("Unimplemented AST_Code_Variable assign type. " . $code_node->assign, $code_node);
							return null;
						break;
					}
				break;

				case AST_Node::CSSPROPERTY:
					$value = self::evaluateCSSExpression($code_node->expr_tokens, $existing_variables, $compiled_codeblock->variables_touched);
					if ($value === null)
					{
						self::compileError("null variable returned from CSS expression.", $code_node);
						return null;
					}
					$compiled_codeblock->variables[$code_node->name] = $value;
				break;

				case AST_Node::IFBLOCK:
				case AST_Node::ELSEIFBLOCK:
					$value = self::evaluateExpression($code_node->expr_tokens, $existing_variables, $compiled_codeblock->variables_touched);
					if (!is_object($value))
					{
						self::compileError("null variable returned from expression in if-block.", $code_node);
						return null;
					}
					if ($value->token_id == Token::TOK_EXPR_BACKEND_VAR || $value->token_id == Token::TOK_EXPR_BACKEND_EXPR)
					{
						self::assert("Backend variable cannot be used in layout if-block yet.", $code_node);
						return null;
					}
					if ($value->string != false)
					{
						// If the statement returned true
						//
						// Remove else/elseif cases from stack
						while (isset($code_queue[0]) &&
							($code_queue[0]->type == AST_Node::ELSEBLOCK || $code_queue[0]->type == AST_Node::ELSEIFBLOCK))
						{
							array_shift($code_queue);
						}

						// Add node children
						$code_queue = array_merge($code_node->nodes, $code_queue);
					}
				break;

				case AST_Node::ELSEBLOCK:
					// Else block only stays on the stack if the previous 'if/else-if' 
					// evaluated to false.
					$code_queue = array_merge($code_node->nodes, $code_queue);
				break;

				default:
					self::compileError("Unsupported code node found in code block. type = '".$code_node->type."'", $ast_codeblock);
					return false;
				break;
			}
		}
		return $compiled_codeblock;
	}

	public static function compileStyles(array $ast_styles, array $existing_variables = array())
	{
		$css = array();
		foreach ($ast_styles as $raw_media_query => $ast_style_media)
		{
			if ($raw_media_query == "@font-face")
			{
				// todo(jake): move font-face into $component->styles_fontface
				foreach ($selector_arr as $font_face)
				{
					$properties = self::compileCodeBlock($font_face->ast_block, $existing_variables);
					$css[$media_query][] = $properties;
				}
			}
			else
			{
				// todo(Jake): convert media query selector_tokens into real @media selector
				foreach ($ast_style_media->ast_styles as $raw_selector_name => $ast_style)
				{
					// Convert selector tokens into real CSS selectors
					$selector_name = "";
					foreach ($ast_style->selectors_tokens as $selector) 
			        {
			            foreach ($selector as $token) 
			            {
			            	if ($token->token_id == Token::TOK_CSS_SELECTOR_COMPONENT)
			            	{
			            		if (!isset(self::$compiled_components[$token->string][""]))
			            		{
			            			self::compileWarning("Component '".$token->string."' referenced 'styles' is not compiled. Selector is ignored", $ast_style);
			            			break 3;
			            		}
			            		$compiled_component = self::$compiled_components[$token->string][""];
			            		if (!empty($compiled_component->attributes["class"]))
			            		{
			            			$selector_name .= str_replace(" ", ".", ".".$compiled_component->attributes["class"]);
			            		}
			            		else if (!empty($compiled_component->element))
			            		{
			            			$selector_name .= $compiled_component->element;
			            		}
			            		else
			            		{
			            			self::assert("Component '".$token->string."' referenced 'styles' has no unique identifier (No element or class variable found).");
			            			return false;
			            		}
			            	}
			            	else
			            	{
			                	$selector_name .= $token->string;
			            	}
			            }
			            $selector_name .= ",";
			        }
			        $selector_name = substr($selector_name, 0, -1);
					if (isset($css[$raw_media_query][$selector_name]))
					{
						self::compileError("Media query and selector has already been declared. media = '" . $raw_media_query."', selector = '".$selector_name."'");
						return false;
					}	
					// todo(Jake): Optimize and ignore CSS that is unused
					//			   $ast_style->selectors_tokens
					$properties = self::compileCodeBlock($ast_style->ast_block, $existing_variables);
					$css[$raw_media_query][$selector_name] = $properties;
				}
			}
		}
		return $css;
	}

	protected static function compileWarning($string, $ast_or_token = null)
	{
		// todo(Jake): store warnings and don't show (maybe only show in JS console)
		if (is_object($ast_or_token))
		{
			if (isset($ast_or_token->filename))
			{
				echo "Compile warning: " . $string . " Line Number: ".$ast_or_token->line_number." on ".$ast_or_token->filename."<br/>";
			}
			else
			{
				echo "Compile warning: " . $string . " Line Number: ".$ast_or_token->line_number."<br/>";
			}
		}
		else
		{
			echo "Compile warning: " . $string . ".<br/>";
		}
	}

	protected static function compileError($string, $ast_or_token = null)
	{
		$dev_info = "";
		if (self::$dev_mode)
		{
			// Get line of where parse error occurred
			$backtrace = debug_backtrace();
			$line = "unknown";
			if (isset($backtrace[0]["line"]))
			{
				$line = $backtrace[0]["line"];
			}
			$dev_info = $line;
			$dev_info = "(".$dev_info.")";
		}

		if (is_object($ast_or_token))
		{
			if (isset($ast_or_token->filename))
			{
				echo "Compile error", $dev_info ,": " . $string . " Line Number: ".$ast_or_token->line_number." on ".$ast_or_token->filename."<br/>";
			}
			else
			{
				echo "Compile error", $dev_info,": " . $string . " Line Number: ".$ast_or_token->line_number."<br/>";
			}
		}
		else
		{
			echo "Compile error", $dev_info,": ", $string, ".<br/>";
		}
		return false;
	}

	protected static function assert($string, $ast = null)
	{
		$stack = xdebug_get_function_stack();
		$prev_stack = end($stack);

		if (is_object($ast))
		{
			$output = "Compile assert error: " . $string . " Code Line Number: ".$prev_stack["line"]. ", Line Number: ".$ast->line_number." on ".$ast->filename."<br/>";
		}
		else
		{
			$output = "Compile assert error: " . $string . ". Code Line Number: ".$prev_stack["line"]. "<br/>";
		}
		echo $output;
		xdebug_print_function_stack($output);
		exit;
	}
}

error_reporting(E_ALL|E_STRICT);
$time = microtime(true);
Compiler::addDirectory(dirname(__FILE__)."/../test/");
Compiler::compile();
/*
$compile_time = microtime(true)-$time;
?>
<head>
	<style>
		<?php echo $compiler->css_output; ?>
	</style>
</head>
<?php echo $compiler->html_output; ?>
<?php */