<?php

// todo(Jake): Make parser static class
class Parser {
    protected static $current_filename;

    protected static $tokens;

    // transient
    protected static $components;

    protected static $enums;

    protected static $force_compile_components = array();

    // misc
    public static $total_parse_time = 0;

    public static $dev_mode = true;

    public static function parse($tokens, $filename = null)
    {
        self::$tokens = $tokens;
        self::$current_filename = $filename;
        self::$components = array();
        self::$enums = array();
        if (empty(self::$tokens)) {
            return false;
        }
        if (isset($_GET["tokens"]))
        {
            self::printTokens($tokens);
        }

        $keep_time = microtime(true);

        $node = new AST_Layout_Element();
        $node->filename = self::$current_filename;
        $node->name = "root";

        $token = current(self::$tokens);
        while ($token !== false)
        {
            if (self::parseComponent() === false && 
                self::parseEnum() === false && 
                self::parseForceCompileComponent() === false)
            {
                if (self::parseElements($node) === false)
                {
                    // Stop compiling if hit error
                    break;
                }
            }
            $token = next(self::$tokens);
        }

        $ast_file = new AST_File();
        $ast_file->layout = $node;
        $ast_file->components = self::$components;
        $ast_file->enums = self::$enums;
        $ast_file->force_compile_components = self::$force_compile_components;
        self::$total_parse_time += (microtime(true) - $keep_time);
        return $ast_file;
    }

    protected static function parseForceCompileComponent()
    {
        $token = current(self::$tokens);
        if ($token !== false && $token->token_id == Token::TOK_KEYWORD && $token->string == "compile")
        {
            $token = next(self::$tokens);
            if ($token->token_id == Token::TOK_KEYWORD)
            {
                self::$force_compile_components[] = $token->string;
            }
            else
            {
                self::parseError("Invalid token after 'compile' keyword, token id = %d", $token);
                return null;
            }
            return true;
        }
        return false;
    }

    protected static function parseEnum()
    {
        $token = current(self::$tokens);
        if ($token->token_id == Token::TOK_KEYWORD && $token->string == "enum")
        {
            $token = next(self::$tokens);
            // todo(Jake): Stop spaces from being in token (do in Lexer)
            if ($token->token_id == Token::TOK_KEYWORD && strpos($token->string, " ") === false)
            {
                $enum = new AST_Enum();
                $enum->filename = self::$current_filename;
                $enum->line_number = $token->line_number;
                $enum->name = $token->string;
                self::$enums[] = $enum;

                $token = next(self::$tokens);
                if ($token->token_id != Token::TOK_BLOCK_OPEN)
                {
                    return self::parseError("Missing opening block character '{' in enum definition.", $token);
                }

                $token = next(self::$tokens);
                while ($token !== false && $token->token_id != Token::TOK_BLOCK_CLOSE)
                {
                    if ($token->token_id !== Token::TOK_EXPR_VAR)
                    {
                        return self::parseError("Expected variable token, unexpected token in enum, token id = %d", $token);
                    }
                    if (isset($enum->variables[$token->string]))
                    {
                        return self::parseError("Cannot define same variable twice '".$token->string."' in '".$enum->name."' enum definition", $token);
                    }
                    $enum->variables[$token->string] = self::parseVariableModify($token);
                    $token = next(self::$tokens);
                }
            }
        }
        return false;
    }

    protected static function parseSelector(AST_Style $ast_style)
    {
        static $begin_token_ids = array(
            AST_Style::TYPE_CSS => Token::TOK_CSS_SELECTOR_BEGIN,
            AST_Style::TYPE_MEDIA => Token::TOK_CSS_MEDIA_BEGIN,
        );
        static $end_token_ids = array(
            AST_Style::TYPE_CSS => Token::TOK_CSS_SELECTOR_END,
            AST_Style::TYPE_MEDIA => Token::TOK_CSS_MEDIA_END,
        );
        static $valid_keywords_set = array(
            AST_Style::TYPE_CSS => array(),
            AST_Style::TYPE_MEDIA => array(
                "only" => 1,
                "screen" => 1,
                "and" => 1,
                "or" => 1,
            ),
        );
        $begin_token_id = $begin_token_ids[$ast_style->type];
        $end_token_id = $end_token_ids[$ast_style->type];
        $valid_keywords = $valid_keywords_set[$ast_style->type];
        $token = current(self::$tokens);
        $is_reading_css_properties = false;
        while ($token !== false && $token->token_id == $begin_token_id)
        {
            $css_selector_tokens = array();
            $token = next(self::$tokens);
            while ($token !== false && $token->token_id != $end_token_id)
            {
                if ($token->token_id == Token::TOK_BRACKET_OPEN) {
                    $is_reading_css_properties = true;
                } else if ($token->token_id == Token::TOK_BRACKET_CLOSE) {
                    $is_reading_css_properties = false;
                } else if (!$is_reading_css_properties &&
                    $token->token_id == Token::TOK_KEYWORD && 
                    !isset($valid_keywords[$token->string]))
                {
                    return self::parseError("Invalid keyword in media query '".$token->string."'", $token);
                }
                $css_selector_tokens[] = $token;
                $token = next(self::$tokens);
            }
            $ast_style->selectors_tokens[] = $css_selector_tokens;
            $token = next(self::$tokens);
        }
        return true;
    }

    protected static function parseComponent()
    {
        $token = current(self::$tokens);
        if ($token->token_id == Token::TOK_KEYWORD && $token->string == "def")
        {
            $token = next(self::$tokens);
            // todo(Jake): Stop spaces from being in token (do in Lexer)
            if ($token->token_id == Token::TOK_KEYWORD && strpos($token->string, " ") === false)
            {
                $component = new AST_Component();
                $component->filename = self::$current_filename;
                $component->line_number = $token->line_number;
                $component->name = $token->string;
                $component->styles = array('' => new AST_Style_Media());
                self::$components[] = $component;

                $token = next(self::$tokens);
                if ($token->token_id != Token::TOK_BLOCK_OPEN)
                {
                    return self::parseError("Missing opening block character '{' in component definition.", $token);
                }

                $token = next(self::$tokens);
                while ($token !== false && $token->token_id != Token::TOK_BLOCK_CLOSE)
                {
                    if ($token->token_id != Token::TOK_KEYWORD)
                    {
                        return self::parseError("Invalid token, '%s' token id = %d", $token);
                    }
                    $block_token = $token;
                    $token = next(self::$tokens);
                    if ($token->token_id != Token::TOK_BLOCK_OPEN)
                    {
                        return self::parseError("Missing opening block character '{' inside component definition. token id = %d, near '" . $block_token->string . "'", $token);
                    }

                    switch ($block_token->string)
                    {
                        case "variables":
                            $component->variables = self::parseCodeBlock($block_token);
                        break;

                        case "attributes":
                            $component->attributes = self::parseCodeBlock($block_token);
                        break;

                        case "calculate":
                            $component->calculate = self::parseCodeBlock($block_token);
                        break;
                        
                        case "styles":
                            // Keep track of style depth
                            // ie. [0] = null, (root)
                            //     [1] = "@media screen and (max-width: 300px)"
                            $style_media_stack = array($component->styles['']);
                            // Read @media {}, self {}, self:hover {} blocks
                            $token = next(self::$tokens);

                            while ($token !== false && $token->token_id != Token::TOK_BLOCK_CLOSE)
                            {
                                if ($token->token_id == Token::TOK_KEYWORD && isset($token->string[0]) && $token->string[0] == "@")
                                {
                                    // @media, @supports, @charset
                                    $special_selector = $token->string;
                                    $token = next(self::$tokens);
                                    
                                    if ($token->token_id == Token::TOK_BLOCK_OPEN)
                                    {
                                        switch ($special_selector)
                                        {
                                            case "@font-face":
                                                //next(self::$tokens);

                                                $style = new AST_Style();
                                                $style->filename = self::$current_filename;
                                                $style->line_number = $token->line_number;
                                                $style->ast_block = self::parseCodeBlock($block_token);
                                                /*foreach ($style->ast_block->nodes as $property)
                                                {
                                                    if ($property->name == "font-family")
                                                    {

                                                        break;
                                                    }
                                                }*/
                                                $component->styles["@font-face"][] = $style;
                                                //return self::parseError("Invalid special selector ".$special_selector.".", $token);
                                            break;

                                            case "@media":
                                            case "@supports":
                                                return self::assert("Special selector parsed wrong.", $token);
                                                /*$style_media_stack[] = $special_selector;
                                                if (isset($style_media_stack[2]))
                                                {
                                                    end($style_media_stack);
                                                    $parent_conditional_selector = prev($style_media_stack);
                                                    return self::parseError("Cannot have ".$special_selector." query inside ".$parent_conditional_selector." query.", $token);
                                                }*/
                                            break;

                                            default:
                                                return self::parseError("Invalid special selector ".$special_selector.".", $token);
                                            break;
                                        }
                                    }
                                    else
                                    {
                                        return self::parseError("Missing opening block character '{' near ".$special_selector." block.", $token);
                                    }
                                }
                                else if ($token->token_id == Token::TOK_CSS_SELECTOR_BEGIN)
                                {
                                    $ast_style = new AST_Style_CSS();
                                    $ast_style->filename = self::$current_filename;
                                    $ast_style->line_number = $token->line_number;
                                    if (!self::parseSelector($ast_style))
                                    {
                                        return null;
                                    }
                                    $token = current(self::$tokens);
                                    //$token = next(self::$tokens);
                                    if ($token->token_id != Token::TOK_BLOCK_OPEN)
                                    {
                                        return self::parseError("Missing opening block character '{' in styles", $token);
                                    }
                                    // todo(jake): cleanup code
                                    $ast_style_media = end($style_media_stack);
                                    // Detect if selector is doubled up
                                    $css_selector = $ast_style->selectorsToString();
                                    if (isset($ast_style_media->ast_styles[$css_selector]))
                                    {
                                        /*if ($media_query_selector == "") {
                                            return self::parseError("Same selector declared twice '".$css_selector."' on component '".$component->name."'", $token);
                                        } else {
                                            return self::parseError("Same selector declared twice '".$css_selector."' in the same media query '".$media_query_selector."' on component '".$component->name."'", $token);
                                        }*/
                                        return self::parseError("Need new error message for clashing css", $token);
                                    }
                                    $ast_style->ast_block = self::parseCodeBlock($block_token);
                                    $ast_style_media->ast_styles[$css_selector] = $ast_style;
                                }
                                else if ($token->token_id == Token::TOK_CSS_MEDIA_BEGIN)
                                {
                                    $ast_style_media = new AST_Style_Media();
                                    $ast_style_media->filename = self::$current_filename;
                                    $ast_style_media->line_number = $token->line_number;
                                    if (!self::parseSelector($ast_style_media))
                                    {
                                        return false;
                                    }
                                    $token = current(self::$tokens);
                                    $style_media_stack[] = $ast_style_media;
                                    $media_query_selector = $ast_style_media->selectorsToString();
                                    $component->styles[$media_query_selector] = $ast_style_media;
                                    if (isset($style_media_stack[2]))
                                    {
                                        end($style_media_stack);
                                        $parent_conditional_selector = prev($style_media_stack);
                                        return self::parseError("Cannot have ".$special_selector." query inside ".$parent_conditional_selector." query.", $token);
                                    }
                                }
                                else
                                {
                                    return self::parseError("Invalid keyword or invalid media query in 'styles', '%s', token id = %d", $token);
                                }
                                
                                $token = next(self::$tokens);

                                // If inside @media/@supports block then exit out of it and keep the loop going
                                if ($token->token_id == Token::TOK_BLOCK_CLOSE && isset($style_media_stack[1]))
                                {
                                    array_pop($style_media_stack);
                                    $token = next(self::$tokens);
                                }
                            }
                        break;

                        case "layout":
                            $layout_node = new AST_Layout_Element();
                            $layout_node->filename = self::$current_filename;
                            $layout_node->name = $component->name;
                            next(self::$tokens);
                            $no_error = self::parseElements($layout_node, $component);
                            if ($no_error === false)
                            {
                                return false;
                            }

                            $component->layout = $layout_node;
                        break;

                        default:

                            return self::parseError("Invalid keyword block '".$block_token->string."' in component.", $token);
                        break;
                    }
                    $token = next(self::$tokens);
                }
                $token = current(self::$tokens);
                if ($token !== false && $token->token_id != Token::TOK_BLOCK_CLOSE)
                {
                    self::parseError("Missing closing block character '}' for '" . $component->name . "' definition.", $token);
                }
                return $component;
            }
            else
            {
                return self::parseError("Invalid component name string , '%s'", $token);
            }
        }
        return false;
    }

    // Parses the layout tokens
    // If component is passed, allow use of component keywords/variables
    protected static function parseElements(AST_Layout $parent_node, AST_Component $component = null)
    {
        $element_stack = array();
        $element_stack[] = $parent_node;
        $token = true;
        while ($token !== false && count($element_stack) > 0)
        {
            $node = end($element_stack);
            $token = current(self::$tokens);
            switch ($token->token_id)
            {
                case Token::TOK_ELEMENT:
                    $element = new AST_Layout_Element();
                    $element->filename = self::$current_filename;
                    $element->line_number = $token->line_number;
                    $element->name = $token->string;
                    $node->nodes[] = $element;

                    if ($component !== null && $element->name == $component->name)
                    {
                        return self::parseError("Cannot use component within itself, use 'self' keyword.", $token);
                    }

                    $token = next(self::$tokens);
                    switch ($token->token_id)
                    {
                        case Token::TOK_BLOCK_OPEN:
                            $element_stack[] = $element;
                            $token = next(self::$tokens); // skip to TOK_ELEMENT / TOK_CONTENT
                        break;

                        case Token::TOK_BRACKET_OPEN:
                            $codeblock = new AST_Block();
                            $codeblock->filename = self::$current_filename;
                            $codeblock->line_number = $token->line_number;
                            $codeblock->name = $token->string;
                            $element->parameters = $codeblock;
                            $token = next(self::$tokens);
                            while ($token !== false && $token->token_id != Token::TOK_BRACKET_CLOSE)
                            {
                                if ($token->token_id == Token::TOK_EXPR_VAR)
                                {
                                    $variable = self::parseVariableModify($token);
                                    $element->parameters->nodes[] = $variable;
                                    if ($variable->expr_tokens === false)
                                    {
                                        // exit if error
                                        return false;
                                    }
                                }
                                else
                                {
                                    return self::parseError("Invalid token (%d), Expected variable name.", $token);
                                }
                                $token = next(self::$tokens);
                            }
                            if ($token == false)
                            {
                                // todo(Jake): handle null tokens
                                $last_token = end(self::$tokens);
                                return self::parseError("Missing closing ')'", $last_token);
                            }

                            $token = next(self::$tokens);
                            if ($token->token_id == Token::TOK_BLOCK_OPEN)
                            {
                                $element_stack[] = $element;
                                next(self::$tokens); // skip to TOK_ELEMENT / TOK_CONTENT
                            }
                            else
                            {
                                return self::parseError("Invalid token (%d), Expecting open block '{'", $token);
                            }
                        break;

                        default:
                            return self::parseError("Invalid token (%d), Expected '{' or '('", $token);
                        break;
                    }
                break;

                case Token::TOK_CONTENT:
                    $element = new AST_Layout_Content();
                    $element->filename = self::$current_filename;
                    $element->line_number = $token->line_number;
                    $element->name = $token->string;
                    $node->nodes[] = $element;
                    next(self::$tokens);
                break;


                case Token::TOK_EXPR_BACKEND_VAR:
                    $element = new AST_Layout_BackendVariable();
                    $element->name = $token->string;
                    $element->filename = self::$current_filename;
                    $element->line_number = $token->line_number;
                    $node->nodes[] = $element;
                    $token = next(self::$tokens);
                break;

                case Token::TOK_KEYWORD:
                    switch ($token->string)
                    {
                        case "else if":
                            $previous_node = end($node->nodes);
                            if ($previous_node === false || $previous_node->type != AST_Node::IFBLOCK)
                            {
                                return self::parseError("'else if' block must proceed an 'if' block", $token);
                            }
                        case "if":
                            $element = self::parseIf();
                            if ($element === false)
                            {
                                // hit error
                                return false;
                            }
                            $node->nodes[] = $element;
                            $element_stack[] = $element;
                            next(self::$tokens);
                        break;

                        case "else":
                            $top_token = $token;
                            $token = next(self::$tokens);
                            if ($token->token_id == Token::TOK_BLOCK_OPEN)
                            {
                                $previous_node = end($node->nodes);
                                if ($previous_node === false || ($previous_node->type != AST_Node::IFBLOCK && $previous_node->type != AST_Node::ELSEIFBLOCK))
                                {
                                    return self::parseError("'else' block must proceed an 'if' or 'else if' block", $top_token);
                                }
                                $element = new AST_Block_Else();
                                $element->filename = self::$current_filename;
                                $element->line_number = $top_token->line_number;
                                $element->name = $top_token->string;

                                $node->nodes[] = $element;
                                $element_stack[] = $element;
                                next(self::$tokens);
                            }
                            else if ($token->token_id == Token::TOK_BLOCK_OPEN)
                            {
                                return self::parseError("Unexpected character in 'else' statement, '(' found instead of '{'", $token);
                            }
                            else
                            {
                                return self::parseError("Unexpected character in 'else' statement, Expected '{'", $token);
                            }
                        break;

                        case "loop":
                            $top_token = $token;
                            $token = next(self::$tokens);
                            switch ($token->token_id)
                            {
                                case Token::TOK_EXPR_BACKEND_VAR:
                                    $array_varname_token = $token;
                                    $token = next(self::$tokens);
                                    $varname_token = next(self::$tokens);
                                    if ($token->token_id != Token::TOK_KEYWORD && $token->string != "as")
                                    {
                                        return self::parseError("Unexpected character in 'loop' statement, Expected 'as' instead of '".$token->string."'", $token);
                                    }
                                    if ($varname_token->token_id != Token::TOK_EXPR_BACKEND_VAR)
                                    {
                                        return self::parseError("Unexpected character in 'loop' statement, Expected backend variable (ie. \${$varname_token->string}) instead of '".$varname_token->string."'", $varname_token);
                                    }
                                    $token = next(self::$tokens);
                                    if ($token->token_id != Token::TOK_BLOCK_OPEN)
                                    {
                                        return self::parseError("Unexpected character in 'loop' statement, Expected '{'", $token);
                                    }
                                    $element = new AST_Block_Loop_Foreach();
                                    $element->filename = self::$current_filename;
                                    $element->line_number = $array_varname_token->line_number;
                                    $element->array_name = $array_varname_token->string;
                                    $element->value_name = $varname_token->string;
                                    $node->nodes[] = $element;
                                    $element_stack[] = $element;
                                    next(self::$tokens);
                                break;

                                case Token::TOK_EXPR_REAL:
                                case Token::TOK_EXPR_REAL_HEX:
                                    $number_token = $token;
                                    $token = next(self::$tokens);
                                    if ($token->token_id == Token::TOK_BLOCK_OPEN)
                                    {
                                        $element = new AST_Block_Loop_Repeat();
                                        $element->filename = self::$current_filename;
                                        $element->line_number = $top_token->line_number;
                                        $element->amount = $number_token->string;
                                        $node->nodes[] = $element;
                                        $element_stack[] = $element;
                                        next(self::$tokens);
                                    }
                                    else
                                    {
                                        return self::parseError("Unexpected character in 'loop' statement, Expected '{'", $token);
                                    }
                                    //return self::assert("single number loop not done.", $top_token);
                                break;

                                default:
                                    return self::parseError("Invalid token in 'loop' declaration.", $top_token);
                                break;
                            }
                        break;

                        case "self":
                            if ($component == null)
                            {
                                return self::parseError("Cannot use 'self' in global layout.", $token);
                            }
                            $element = new AST_Layout_Element();
                            $element->filename = self::$current_filename;
                            $element->line_number = $token->line_number;
                            $element->name = $token->string;
                            $node->nodes[] = $element;
                            $token = next(self::$tokens);
                            switch ($token->token_id)
                            {
                                case Token::TOK_BLOCK_OPEN:
                                    $element_stack[] = $element;
                                    $token = next(self::$tokens); // skip to TOK_ELEMENT / TOK_CONTENT
                                break;

                                default:
                                    return self::parseError("Invalid token (%d) near 'self', Expected '{'", $token);
                                break;
                            }
                        break;

                        default:
                            if ($component == null)
                            {
                                return self::parseError("Invalid keyword (%s)", $token);
                            }
                            $element = new AST_Layout_Variable();
                            $element->name = $token->string;
                            $element->filename = self::$current_filename;
                            $element->line_number = $token->line_number;
                            $node->nodes[] = $element;
                            $token = next(self::$tokens);
                        break;
                    }
                break;

                case Token::TOK_BLOCK_OPEN:
                    // keyword-less block open
                    $element_stack[] = end($element_stack);
                    $token = next(self::$tokens);
                break;

                case Token::TOK_BLOCK_CLOSE:
                    if (count($element_stack) > 1)
                    {
                        array_pop($element_stack);
                        $token = next(self::$tokens);
                    }
                    else
                    {
                        // If back to first element found being closed, exit.
                        return true;
                    }
                break;

                default:
                    self::assert("ParseElement: Invalid token (%d), Expected '{' or '(', string = %s", $token);
                    return null;
                break;
            }
        }
        if (count($element_stack) > 1)
        {
            return self::parseError("Too many closing '}' brackets in scope.");
        }
        return true;
    }

    protected static function skipWhitespace()
    {
        $token = current(self::$tokens);
        while ($token !== false && $token->token_id == Token::TOK_WHITESPACE)
        {
            $token = next(self::$tokens);
        }
        prev(self::$tokens);
    }

    protected static function parseCodeBlock(Token $block_token)
    {
        $codeblock_root = new AST_Block();
        $codeblock_root->filename = self::$current_filename;
        $codeblock_root->line_number = $block_token->line_number;
        $codeblock_root->name = $block_token->string;

        $codeblock_stack = array();
        $codeblock_stack[] = $codeblock_root;

        $token = next(self::$tokens);

        while (count($codeblock_stack) > 0)
        {
            $codeblock = end($codeblock_stack);
            if ($token !== false && $token->token_id != Token::TOK_BLOCK_CLOSE)
            {
                switch ($token->token_id)
                {
                    case Token::TOK_EXPR_CSS_PROP:
                        $assign_token = next(self::$tokens);
                        if ($assign_token->token_id != Token::TOK_EXPR_SET)
                        {
                            return self::parseError("Invalid CSS set.", $assign_token);
                        }

                        $node = new AST_Code_CSSProperty();
                        $node->filename = self::$current_filename;
                        $node->line_number = $token->line_number;
                        $node->name = $token->string;
                        $node->expr_tokens = self::readUntilExpressionEnd();
                        $codeblock->nodes[] = $node;
                    break;

                    case Token::TOK_EXPR_VAR:
                        $codeblock->nodes[] = self::parseVariableModify($token);
                    break;

                    case Token::TOK_KEYWORD:
                        switch ($token->string)
                        {
                            case "else if":
                                $previous_node = end($codeblock->nodes);
                                if ($previous_node === false || $previous_node->type != AST_Node::IFBLOCK)
                                {
                                    return self::parseError("'else if' block must proceed an 'if' block", $token);
                                }
                            case "if":
                            case "else if":
                                $next_codeblock = self::parseIf();
                                if ($next_codeblock === false)
                                {
                                    // hit error
                                    return false;
                                }
                                $codeblock->nodes[] = $next_codeblock;
                                $codeblock_stack[] = $next_codeblock;   
                            break;

                            case "else":
                                $top_token = $token;
                                $token = next(self::$tokens);
                                if ($token->token_id == Token::TOK_BLOCK_OPEN)
                                {
                                    $previous_node = end($codeblock->nodes);
                                    if ($previous_node === false || ($previous_node->type != AST_Node::IFBLOCK && $previous_node->type != AST_Node::ELSEIFBLOCK))
                                    {
                                        return self::parseError("'else' block must proceed an 'if' or 'else if' block", $top_token);
                                    }
                                    $next_codeblock = new AST_Block_Else();
                                    $next_codeblock->filename = self::$current_filename;
                                    $next_codeblock->line_number = $top_token->line_number;
                                    $next_codeblock->name = $top_token->string;
                                    $codeblock->nodes[] = $next_codeblock;
                                    $codeblock_stack[] = $next_codeblock;
                                }
                                else if ($token->token_id == Token::TOK_BLOCK_OPEN)
                                {
                                    return self::parseError("Unexpected character in 'else' statement, '(' found instead of '{'", $token);
                                }
                                else
                                {
                                    return self::parseError("Unexpected character in 'else' statement, Expected '{'", $token);
                                }
                            break;

                            default:
                                return self::parseError("Unknown keyword token '%s' in code block", $token);
                            break;
                        }
                    break;

                    case Token::TOK_COMMENT_ONE_LINE:
                    case Token::TOK_COMMENT_BLOCK:
                        // ignore comments
                    break;

                    default:
                        self::assert("Unknown token id = %d in code block", $token);
                    break;
                }
                $token = next(self::$tokens);
            }
            else
            {
                array_pop($codeblock_stack);
                $token = next(self::$tokens);
            }
        }
        // Go back to block end token
        prev(self::$tokens);
        return $codeblock_root;
    }

    protected static function parseIf()
    {
        $top_token = current(self::$tokens);
        $token = next(self::$tokens);
        if ($token->token_id == Token::TOK_BRACKET_OPEN)
        {
            $tokens = self::parseExpression();
            if ($tokens === false)
            {
                // exit if error
                return false;
            }
            $token = next(self::$tokens);
            if ($token->token_id == Token::TOK_BRACKET_CLOSE)
            {
                $token = next(self::$tokens);
                if ($token->token_id == Token::TOK_BLOCK_OPEN)
                {
                    if ($top_token->string == "if") {
                        $next_codeblock = new AST_Block_If();
                    } else if ($top_token->string == "else if") {
                        $next_codeblock = new AST_Block_ElseIf();
                    }
                    $next_codeblock->filename = self::$current_filename;
                    $next_codeblock->line_number = $top_token->line_number;
                    $next_codeblock->name = $top_token->string;
                    $next_codeblock->expr_tokens = $tokens;
                    return $next_codeblock;
                }
                else
                {
                    return self::parseError("Unexpected character after 'if' statement, Expected '{' to open the if block", $token);
                }
            }
            else
            {
                return self::parseError("Unexpected character in 'if' statement, Expected ')' to close the expression", $token);
            }
        }
        else
        {
            return self::parseError("Unexpected character in 'if' statement, Expected '(' before expression", $token);
        }
        return self::assert("Unknown error in if statement.", $top_token);
    }

    // ie. setting, adding to, concatting, subtracting
    protected static function parseVariableModify(Token $token)
    {
        $node = new AST_Code_Variable();
        $node->filename = self::$current_filename;
        $node->line_number = $token->line_number;
        $node->name = $token->string;

        $assign_token = next(self::$tokens);
        switch ($assign_token->token_id)
        {
            case Token::TOK_EXPR_SET:
                $node->assign = AST_Code_Variable::SET;
            break;

            case Token::TOK_EXPR_ADD:
                $node->assign = AST_Code_Variable::ADD;
            break;

            default:
                self::parseError("Invalid variable assign operator token '%s' in code block", $assign_token);
            break;
        }

        $node->expr_tokens = self::parseExpression();
        return $node;
    }

    protected static function parseExpression()
    {
        static $operator_precedence = array(
            Token::TOK_BRACKET_OPEN => 1,
            Token::TOK_BRACKET_CLOSE => 1,
            Token::TOK_EXPR_COND_AND => 2,
            Token::TOK_EXPR_COND_OR => 2,
            Token::TOK_EXPR_COND_EQUAL => 3,
            Token::TOK_EXPR_COND_NOT_EQUAL => 3,
            Token::TOK_EXPR_COND_ABOVE_EQUAL => 3,
            Token::TOK_EXPR_COND_BELOW_EQUAL => 3,
            Token::TOK_EXPR_COND_ABOVE => 3,
            Token::TOK_EXPR_COND_BELOW => 3,
            Token::TOK_EXPR_ADD => 4,
            Token::TOK_EXPR_SUB => 4,
            Token::TOK_EXPR_MULT => 5,
            Token::TOK_EXPR_DIV => 5,
            Token::TOK_EXPR_MOD => 5,
        );

        $operator_stack = array();
        $postfix_list = array();

        $has_variable = false;
        $context_token = prev(self::$tokens); next(self::$tokens);
        $token = next(self::$tokens);
        while ($token !== false && $token->token_id !== Token::TOK_EXPR_END)
        {
            switch ($token->token_id)
            {
                case Token::TOK_EXPR_BACKEND_VAR:
                    $postfix_list[] = $token;
                break;

                case Token::TOK_EXPR_VAR:
                    $has_variable = true;
                case Token::TOK_EXPR_STR:
                case Token::TOK_EXPR_REAL:
                case Token::TOK_EXPR_REAL_HEX:
                    $postfix_list[] = $token;
                break;

                case Token::TOK_BRACKET_OPEN:
                    $operator_stack[] = $token;
                break;

                case Token::TOK_BRACKET_CLOSE:
                    $top_operator = array_pop($operator_stack);
                    while (is_object($top_operator) && $top_operator->token_id != Token::TOK_BRACKET_OPEN)
                    {
                        $postfix_list[] = $top_operator;
                        $top_operator = array_pop($operator_stack);
                    }
                break;

                case Token::TOK_BLOCK_OPEN:
                    if ($context_token->token_id == 6)
                    {
                        switch ($context_token->string)
                        {
                            case "if":
                                self::parseError("Unexpected character '{', expected ')' for 'if' statement.", $token);
                            break;

                            case "else if":
                                self::parseError("Unexpected character '{', expected ')' for 'else if' statement.", $token);
                            break;

                            default:
                                self::parseError("Unexpected character '{' in expression near '".$context_token->string."' keyword.", $token);
                            break;
                        }
                    }
                    else
                    {
                        self::parseError("Unexpected character '{' in expression.", $token);
                    }
                    return false;
                break;

                /*case Token::TOK_EXPR_COND_EQUAL:
                    $top_operator = array_pop($operator_stack);
                    while (is_object($top_operator) && ( 
                            $top_operator->token_id != Token::TOK_EXPR_COND_EQUAL &&
                            $top_operator->token_id != Token::TOK_EXPR_COND_AND &&
                            $top_operator->token_id != Token::TOK_EXPR_COND_OR))
                    {
                        $postfix_list[] = $top_operator;
                        $top_operator = array_pop($operator_stack);
                    }
                    $operator_stack[] = $token;
                break;

                case Token::TOK_EXPR_COND_AND:
                case Token::TOK_EXPR_COND_OR:
                    $top_operator = array_pop($operator_stack);
                    while (is_object($top_operator) && ( 
                            $top_operator->token_id != Token::TOK_EXPR_COND_AND &&
                            $top_operator->token_id != Token::TOK_EXPR_COND_OR))
                    {
                        $postfix_list[] = $top_operator;
                        $top_operator = array_pop($operator_stack);
                    }
                    $operator_stack[] = $token;
                break;*/

                case Token::TOK_EXPR_COND_EQUAL:
                case Token::TOK_EXPR_COND_NOT_EQUAL:
                case Token::TOK_EXPR_COND_ABOVE_EQUAL:
                case Token::TOK_EXPR_COND_BELOW_EQUAL:
                case Token::TOK_EXPR_COND_ABOVE:
                case Token::TOK_EXPR_COND_BELOW:
                case Token::TOK_EXPR_COND_AND:
                case Token::TOK_EXPR_COND_OR:
                case Token::TOK_EXPR_ADD:
                case Token::TOK_EXPR_SUB:
                case Token::TOK_EXPR_MULT:
                case Token::TOK_EXPR_DIV:
                case Token::TOK_EXPR_MOD:
                    while (count($operator_stack) > 0)
                    {
                        $top_operator = end($operator_stack);
                        $top_prec = $operator_precedence[$top_operator->token_id];
                        $this_prec = $operator_precedence[$token->token_id];
                        
                        if ($top_prec >= $this_prec)
                        {
                            $postfix_list[] = array_pop($operator_stack);
                        }
                        else
                        {
                            break;
                        }
                    }
                    $operator_stack[] = $token;
                break;

                default:
                    return self::parseError("ParseExpression::Bad token in expression, token id = %d, value = '%s'", $token);
                break;
            }
            $token = next(self::$tokens);
        }
        while (count($operator_stack) > 0)
        {
            $postfix_list[] = array_pop($operator_stack);
        }

        // If there are no variable references in the expression
        // evaluate it.
        if (!$has_variable)
        {
            foreach ($postfix_list as $token)
            {
                if ($token->token_id >= Token::TOK_EXPR_SET && $token->token_id <= Token::TOK_EXPR_COND_EQUAL)
                {
                    $rval = array_pop($stack);
                    $lval = array_pop($stack);
                    $stack[] = Compiler::compute($lval, $rval, $token->token_id);
                }
                else
                {
                    $stack[] = $token;
                }
            }
            if (count($stack) > 1 || count($stack) == 0)
            {
                return self::parseError("Unable to pre-evaluate expression.", $token);
            }
            else 
            {
                $postfix_list = $stack;
            }
        }
        return $postfix_list;
    }

    public static function postfixArrToString(array $token_arr)
    {
        static $tok_to_str = array(
            Token::TOK_EXPR_ADD => "+",
            Token::TOK_EXPR_SUB => "-",
            Token::TOK_EXPR_MULT => "*",
            Token::TOK_EXPR_DIV => "/",
            Token::TOK_EXPR_MOD => "%",
            Token::TOK_BRACKET_OPEN => "(",
            Token::TOK_BRACKET_CLOSE => ")",
            Token::TOK_EXPR_COND_AND => "&&",
            Token::TOK_EXPR_COND_OR => "||",
            Token::TOK_EXPR_COND_EQUAL => "=="
        );

        $str = "";
        foreach ($token_arr as $token)
        {
            if (isset($tok_to_str[$token->token_id]))
            {
                $str .= $tok_to_str[$token->token_id];
            }
            else
            {
                switch ($token->token_id)
                {
                    case Token::TOK_EXPR_STR:
                        $str .= "\"".$token->string."\"";
                    break;

                    case Token::TOK_EXPR_VAR:
                    case Token::TOK_EXPR_REAL:
                    case Token::TOK_EXPR_REAL_HEX:
                        $str .= $token->string;
                    break;

                    default:
                        var_dump("Parser::postfixArrToString: Invalid token used '".$token->string."' at Line " . $token->line_number);
                        return null;
                    break;
                }
            }
            $str .= " ";
        }
        return $str;
    }

    protected static function printTokens(array $tokens)
    {
        static $NEWLINE = "\n\r<br/>";
        static $INDENT = "&nbsp;&nbsp;&nbsp;&nbsp;";
        static $INDENT_SIZE = 24;

        // If single token
        if (is_object($tokens))
        {
            $tokens = array($tokens);
        }

        $current_indent = "";
        $output = "";
        foreach ($tokens as $token)
        {
            switch ($token->token_id)
            {
                case Token::TOK_KEYWORD:
                case Token::TOK_ELEMENT:
                    $output .= $current_indent;
                    $output .= $token->string . $NEWLINE;
                break;

                case Token::TOK_BRACKET_OPEN:
                    $output .= "(";
                break;

                case Token::TOK_BRACKET_CLOSE:
                    $output .= ")";
                break;

                case Token::TOK_BLOCK_OPEN:
                    $output .= $current_indent;
                    $output .= "{" . $NEWLINE;
                    $current_indent .= $INDENT;
                break;

                case Token::TOK_BLOCK_CLOSE:
                    $current_indent = substr($current_indent, 0, -$INDENT_SIZE);
                    $output .= $current_indent;
                    $output .= "}" . $NEWLINE;
                break;

                case Token::TOK_CONTENT:
                    $output .= "\"".$token->string."\"";
                break;

                case Token::TOK_EXPR_CSS_PROP:
                    $output .= $current_indent;
                case Token::TOK_EXPR_CSS_VAL:
                case Token::TOK_EXPR_REAL:
                case Token::TOK_EXPR_REAL_HEX:
                    $output .= $token->string;
                break;

                case Token::TOK_EXPR_BACKEND_VAR:
                    $output .= "$".$token->string;
                break;

                case Token::TOK_EXPR_BACKEND_EXPR:
                    $output .= "(".$token->string.")";
                break;

                case Token::TOK_EXPR_VAR:
                    $output .= "v:".$token->string;
                break;

                case Token::TOK_EXPR_STR:
                    $output .= "\"".$token->string."\"";
                break;

                case Token::TOK_EXPR_COND_EQUAL:
                case Token::TOK_EXPR_COND_NOT_EQUAL:
                    $output .= Token::operatorToString($token);
                break;

                case Token::TOK_EXPR_CSS_VAR:
                    $output .= "@".$token->string;
                break;

                case Token::TOK_EXPR_SET:
                    $output .= ": ";
                break;

                case Token::TOK_EXPR_END:
                    $output .= "<expr_end>" . $NEWLINE;
                break;

                case Token::TOK_CSS_SELECTOR_BEGIN:
                    $output .= "|cssselector_start|";
                break;

                case Token::TOK_CSS_SELECTOR_END:
                    $output .= "|cssselector_end|";
                break;

                case Token::TOK_CSS_SELECTOR_CHILD:
                case Token::TOK_CSS_SELECTOR_ADJACENT:
                case Token::TOK_CSS_SELECTOR_SIBLING:
                case Token::TOK_CSS_SELECTOR_ANCESTOR:
                case Token::TOK_CSS_SELECTOR_ELEMENT:
                case Token::TOK_CSS_SELECTOR_CLASS:
                    $output .= $token->string;
                break;

                default:
                    $output .= print_r($token, true);
                break;
            }
        }
        echo $NEWLINE.$output;
        return $output;
    }

    protected static function readUntilExpressionEnd()
    {
        $expr_tokens = array();
        $token = next(self::$tokens);
        while ($token !== false && $token->token_id !== Token::TOK_EXPR_END)
        {
            if ($token->token_id >= Token::TOK_EXPR && $token->token_id <= Token::TOK_EXPR_END)
            {
                $expr_tokens[] = $token;
                $token = next(self::$tokens);
            }
            else
            {   
                self::parseError("Bad token in expression, token id = %d", $token);
                return null;
            }
        }
        return $expr_tokens;
    }

    protected static function assert($string, $token = null) {
        $dev_info = "";
        if (self::$dev_mode)
        {
            // Get line of where parse error occurred
            $backtrace = debug_backtrace();
            $line = "unknown";
            if (isset($backtrace[0]["line"]))
            {
                $line = $backtrace[0]["line"];
            }
            $dev_info = $line;
            $dev_info = "(".$dev_info.")";
        }

        $string = str_replace("%d", (int)$token->token_id, $string);
        $string = str_replace("%s", $token->string, $string);
        echo "Parser assert error", $dev_info, ": " . $string . ". Line Number " . $token->line_number . " in file : ". self::$current_filename." <br/>"; 
        exit;
    }

    protected static function parseError($string, $token = null) {
        $dev_info = "";
        if (self::$dev_mode)
        {
            // Get line of where parse error occurred
            $backtrace = debug_backtrace();
            $line = "unknown";
            if (isset($backtrace[0]["line"]))
            {
                $line = $backtrace[0]["line"];
            }
            $dev_info = $line;
            $dev_info = "(".$dev_info.")";
        }

        $string = str_replace("%d", (int)$token->token_id, $string);
        $string = str_replace("%s", $token->string, $string);
        echo "Parser error", $dev_info, ": ", $string, ". Line Number ", $token->line_number, " in file : ", self::$current_filename, " <br/>"; 
        return false;
    }
}