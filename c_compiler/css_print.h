#ifndef CSS_PRINT_INCLUDE
#define CSS_PRINT_INCLUDE

#include "css.h"

void printCSSRule(Buffer& buffer, CSS_Rule* absoluteTopCSSRule)
{
	if (absoluteTopCSSRule->selectorSets->used == 0)
	{
		return;
	}

	for (s32 i = 0; i < absoluteTopCSSRule->selectorSets->used; ++i)
	{
		if (i != 0)
		{
			buffer.add(",\n");
		}
		Array<CSS_Selector>* selectors = &absoluteTopCSSRule->selectorSets->data[i];
		for (s32 i = 0; i < selectors->used; ++i)
		{
			CSS_Selector* selector = &selectors->data[i];
			if (i != 0 && selector->type != CSS_SELECTOR_ATTRIBUTE)
			{
				buffer.add(" ");
			}
			if (selector->type == CSS_SELECTOR_ATTRIBUTE)
			{
				buffer.add("[%s=", &selector->attribute.name, &selector->attribute.op);
				if (selector->attribute.value.type == TOKEN_STRING) {
					buffer.add("\"%s\"", &selector->attribute.value);
				} else {
					buffer.add("%s", &selector->attribute.value);
				}
				buffer.add(']');
				buffer.print();
			}
			else
			{
				buffer.add("%s", &selector->token);
			}
		}
	}

	buffer.add(" {");
	++buffer.indent;
	buffer.addNewline();

	for (s32 p = 0; p < absoluteTopCSSRule->properties->used; ++p)
	{
		if (p != 0)
		{
			buffer.addNewline();
		}
		CSS_Property* cssProperty = &absoluteTopCSSRule->properties->data[p];
		buffer.add("%s: ", &cssProperty->name);
		for (s32 i = 0; i < cssProperty->tokens->used; ++i)
		{
			if (i != 0)
			{
				buffer.add(' ');
			}
			CSS_PropertyToken propToken = cssProperty->tokens->data[i];
			buffer.add("%s", &propToken.token);
			if (propToken.arguments != NULL && propToken.arguments->used > 0)
			{
				buffer.add("(");
				for (s32 i = 0; i < propToken.arguments->used; ++i)
				{
					if (i != 0) {
						buffer.add(",");
					}
					buffer.add("%s", &propToken.arguments->data[i]);
				}
				buffer.add(")");
			}
		}
		buffer.add(";");
	}

	--buffer.indent;
	buffer.addNewline();
	buffer.add("}");
}

#endif